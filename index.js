const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const route = express.Router();
const { authRouter } = require('./auth/auth.router');
const port = 3000;

//set respond json
const jsonParser = bodyParser.json();

//set view, css
app.use(express.static(__dirname + '/public/chapter3'));
app.use(express.static(__dirname + '/public/chapter4'));
app.set('view engine', 'html');
app.engine('html', require('ejs').renderFile);
app.set('views', './views');

//route to home page
app.get('/', jsonParser, (req, res) => {
  res.render('chapter3/index');
});

//route to game page
app.get('/rock-paper-scissors-game', jsonParser, (req, res) => {
  res.render('chapter4/index');
});

//route to login
app.use(authRouter);

app.listen(port, () => {
  console.log(`App listening to http://localhost:${port}`);
});
